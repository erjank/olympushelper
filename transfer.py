import sys
from os import path, listdir, remove, mkdir, makedirs
from subprocess import call

camdir="/Volumes/Untitled/DCIM/101OLYMP/"
pat = "{:06d}"
picdirs = [int(i) for i in listdir('pictures') if i[0]!='.']
newdir = pat.format(max(picdirs)+1)
makedirs('pictures/'+newdir+"/jpg")
makedirs('pictures/'+newdir+"/raw")
call("cp "+ camdir+"*.JPG pictures/"+newdir+"/jpg/.",shell=True)
call("cp "+ camdir+"*.ORF pictures/"+newdir+"/raw/.",shell=True)
