import sys
from os import path, listdir, remove

pat = "{:06d}"
d=pat.format(int(sys.argv[1]))
h = path.abspath('./pictures')
jpg = path.join(h,d,'jpg')
raw = path.join(h,d,'raw')
if not path.isdir(raw):
    exit(1)
jpegs= [i.split('.')[0] for i in listdir(jpg) if i[0]=='P']
raws= [i.split('.')[0] for i in listdir(raw) if i[0]=='P']
to_remove = []
print("Shall I delete:")
for r in raws:
    if r not in jpegs:
        to_remove.append(path.join(h,d,raw,r+'.ORF'))
        print(to_remove[-1])
a = input("\nY/N?")
if a =="Y":
    print("OK!")
    for i in to_remove:
        remove(i)
else:
    print("Not deleting anything.")


