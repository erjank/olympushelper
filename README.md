These two python scripts might be handy if you have a camera like an Olympus OM-D EM-5.
You can take super high quality photos with this thing, but then you have the problem
of managing these large (~10MB for the raw images) files for archival purposes (e.g.,
grandma wants a framed picture of the newborn, but doesn't ask until he turns 3), and
the competing challenge of wanting to share the good images with friends and family.
Another issue is that you can easily take hundreds of photos in an hour, and lots of
them are out of focus if, like Eric, you are not great at cameras. 
This is a problem, because even for software like Olympus' image manipulation program,
it takes a long time to load and display the large raw files.

So, how do we meet the competing demands of small-but-passable images for emails/facebook,
saving giant-but-high-resolution images for precious memories, AND automate the deletion of
the raw files we don't want to save?

Eric's workflow with his camera is something like this:

* Set the camera so it simultaneously saves a (small) jpg and (very large) raw image for each photo taken
* Use `transfer.py` to move the pictures from the camera's SD card onto your laptop 
    * Running `$ python transfer.py` will create a new directory for the photos from your most recent session. 
* Use your favorite viewer to look at the JPG files, and delete the ones that are terrible (Eric uses `Preview`)
* Use `clean_up.py` to automatically delete the local .RAW files corresponding to the deleted JPGs.
* Move the RAW files to your favorite archival storage spot so as not to fill your laptop's disk (e.g., external drives)
* Put the JPGs on facebook/dropbox, show off new vocabulary like "depth-of-field".
* Delete everything on the SD Card and stick it back in the camera.
* Don't forget to charge your camera's battery or batteries!